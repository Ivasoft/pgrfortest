package io.github.oleIva.utils;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author Oleh Ivashko
 */
public class DatabaseFactory {
    private static Properties properties;

    public static Connection createConnection() throws SQLException {
        return DriverManager.getConnection(URL, USER, PASSWORD);
    }

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    static { properties = new Properties();
        try {
            properties.load(DatabaseFactory.class.getResourceAsStream("/application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static final String URL = properties.getProperty("url");
    private static final String USER = properties.getProperty("user");
    private static final String PASSWORD = properties.getProperty("password");

}
