package io.github.oleIva.utils;


import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Oleh Ivashko
 */
public class DataFormatPojo {

    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm";
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);

    public static String getFormattedDate(LocalDateTime date) {
        return date.format(formatter);
    }

    public static LocalDateTime createDate(String unformatted) throws ParseException {
        return LocalDateTime.parse(unformatted, formatter);
    }
}
