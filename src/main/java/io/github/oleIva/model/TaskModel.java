package io.github.oleIva.model;

import java.time.LocalDateTime;

import io.github.oleIva.model.enums.PriorityEnum;
import io.github.oleIva.model.enums.StatusEnum;

/**
 * @author Oleh Ivashko
 */
public class TaskModel {

    private int id;
    private String name;
    private LocalDateTime completeTime;
    private PriorityEnum priority;
    private StatusEnum taskStatus;

    public TaskModel(String name, LocalDateTime completeTime, PriorityEnum prioritet, StatusEnum taskStatus) {
        this.name = name;
        this.completeTime = completeTime;
        this.priority = prioritet;
        this.taskStatus = taskStatus;
    }

    public TaskModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(LocalDateTime completeTime) {
        this.completeTime = completeTime;
    }

    public PriorityEnum getPriority() {
        return priority;
    }

    public void setPriority(PriorityEnum priority) {
        this.priority = priority;
    }

    public StatusEnum getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(StatusEnum taskStatus) {
        this.taskStatus = taskStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskModel task = (TaskModel) o;

        if (id != task.id) return false;
        if (name != null ? !name.equals(task.name) : task.name != null) return false;
        if (completeTime != null ? !completeTime.equals(task.completeTime) : task.completeTime != null) return false;
        if (priority != task.priority) return false;
        return taskStatus == task.taskStatus;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (completeTime != null ? completeTime.hashCode() : 0);
        result = 31 * result + (priority != null ? priority.hashCode() : 0);
        result = 31 * result + (taskStatus != null ? taskStatus.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TaskModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", completeTime=" + completeTime +
                ", priority=" + priority +
                ", taskStatus=" + taskStatus +
                '}';
    }
}
