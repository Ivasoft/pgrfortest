package io.github.oleIva.model.enums;

/**
 * @author Oleh Ivashko
 */
public enum StatusEnum {
    NEW(),
    COMPLETE(),
    OVERDUE()
}
