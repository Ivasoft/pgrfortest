package io.github.oleIva.model.enums;

/**
 * @author Oleh Ivashko
 */
public enum PriorityEnum {
    HIGH(),
    NORMAL(),
    LOW()
}
