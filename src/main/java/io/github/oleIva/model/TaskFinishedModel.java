package io.github.oleIva.model;

import io.github.oleIva.model.enums.PriorityEnum;
import io.github.oleIva.model.enums.StatusEnum;

import java.time.LocalDateTime;

/**
 * @author Oleh Ivashko
 */

public class TaskFinishedModel {
    private int id;
    private String name;
    private LocalDateTime completeTime;
    private PriorityEnum priority;
    private StatusEnum taskStatus;

    public TaskFinishedModel(String name, LocalDateTime completeTime, PriorityEnum priority, StatusEnum taskStatus) {
        this.id = id;
        this.name = name;
        this.completeTime = completeTime;
        this.priority = priority;
        this.taskStatus = taskStatus;
    }

    public TaskFinishedModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(LocalDateTime completeTime) {
        this.completeTime = completeTime;
    }

    public PriorityEnum getPriority() {
        return priority;
    }

    public void setPriority(PriorityEnum priority) {
        this.priority = priority;
    }

    public StatusEnum getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(StatusEnum taskStatus) {
        this.taskStatus = taskStatus;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskFinishedModel that = (TaskFinishedModel) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (completeTime != null ? !completeTime.equals(that.completeTime) : that.completeTime != null) return false;
        if (priority != that.priority) return false;
        return taskStatus == that.taskStatus;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (completeTime != null ? completeTime.hashCode() : 0);
        result = 31 * result + (priority != null ? priority.hashCode() : 0);
        result = 31 * result + (taskStatus != null ? taskStatus.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TaskModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", completeTime=" + completeTime +
                ", priority=" + priority +
                ", taskStatus=" + taskStatus +
                '}';
    }
}
