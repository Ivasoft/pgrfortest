package io.github.oleIva.services;

import io.github.oleIva.model.TaskFinishedModel;
import io.github.oleIva.model.TaskModel;
import io.github.oleIva.model.enums.PriorityEnum;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Oleh Ivashko
 */
public interface TaskService {

    TaskModel createTask(String name, LocalDateTime completeDate, PriorityEnum taskPriority);

    void completeTask(int id);

    List<TaskModel> getAllTasks();

    List<TaskFinishedModel> getAllCompleteTasks();
}
