package io.github.oleIva.services;

import io.github.oleIva.dao.Dao;
import io.github.oleIva.model.TaskFinishedModel;
import io.github.oleIva.model.TaskModel;
import io.github.oleIva.model.enums.PriorityEnum;
import io.github.oleIva.model.enums.StatusEnum;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Oleh Ivashko
 */
public class TaskServiceImpl implements TaskService {

    private Dao tasksDao;

    public TaskServiceImpl(Dao tasksDao) {
        this.tasksDao = tasksDao;
    }

    @Override
    public TaskModel createTask(String name, LocalDateTime completeDate, PriorityEnum taskPriority) {
        TaskModel task = new TaskModel(name, completeDate, taskPriority, StatusEnum.NEW);
        return tasksDao.createTask(task);
    }

    @Override
    public List<TaskModel> getAllTasks() {
        List<TaskModel> taskList = tasksDao.getAllTasks();
        List<TaskModel> taskListNoComplete = new ArrayList<>();
        for (TaskModel task : taskList) {
            if (!task.getTaskStatus().equals(StatusEnum.COMPLETE)) {
                if (task.getCompleteTime().compareTo(LocalDateTime.now()) < 0) {
                    task.setTaskStatus(StatusEnum.OVERDUE);
                }
                taskListNoComplete.add(task);
            }
        }
        return taskListNoComplete;
    }


    @Override
    public List<TaskFinishedModel> getAllCompleteTasks() {
        List<TaskFinishedModel> tasksWithState = new ArrayList<>();
        List<TaskFinishedModel> allTasksList = tasksDao.getAllFinishedTasks();

        for (TaskFinishedModel task : allTasksList) {
            if (task.getTaskStatus().equals(StatusEnum.COMPLETE)) {
                tasksWithState.add(task);
            }
        }
        return tasksWithState;
    }


    /**
     * 1. Find task dao
     * 2. Create in Finished Ent
     * 3. Delete task in TaskEntity
     */
    @Override
    public void completeTask(int id) {
        TaskModel completedTask = tasksDao.getTask(id);
        TaskFinishedModel taskFinished = new TaskFinishedModel(
                completedTask.getName(),
                completedTask.getCompleteTime(),
                completedTask.getPriority()
                , StatusEnum.COMPLETE);
        tasksDao.createCompleteTask(taskFinished);
        tasksDao.deleteFromTasks(completedTask);
    }

}
