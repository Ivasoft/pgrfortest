package io.github.oleIva.dao;

/**
 * @author Oleh Ivashko
 */
public abstract class Queries {
    String CREATE_TASK =
            "INSERT INTO Tasks(" +
                    "name," +
                    "complete_date," +
                    "priority_id," +
                    "status_id )" +
                    "VALUES (?,?,?,?)";

    String CREATE_COMPLETE_TASK =
            "INSERT INTO TaskFinished(" +
                    "name," +
                    "complete_date," +
                    "priority_id," +
                    "status_id )" +
                    "VALUES (?,?,?,?)";

    String GET_TASK =
            "SELECT  t.name," +
                    "t.complete_date," +
                    "t.priority_id," +
                    "t.status_id " +
                    "FROM Tasks t WHERE t.id='%d'";

    String UPDATE_TASK =
            "UPDATE Tasks SET " +
                    "name=?," +
                    "complete_date=?," +
                    "priority_id=?," +
                    "status_id=? " +
                    "WHERE id=?";

    String GET_ALL_TASKS =
            "SELECT  t.id," +
                    "t.name," +
                    "t.complete_date," +
                    "t.priority_id," +
                    "t.status_id " +
                    "FROM Tasks t ";

    String GET_ALL_TASKS_FINISH =
            "SELECT  t.id," +
                    "t.name," +
                    "t.complete_date," +
                    "t.priority_id," +
                    "t.status_id " +
                    "FROM Taskfinished t ";

    String DELETE_TASK = "DELETE FROM Tasks WHERE id=?";

}
