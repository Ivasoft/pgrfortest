package io.github.oleIva.dao;

import io.github.oleIva.model.TaskFinishedModel;
import io.github.oleIva.model.TaskModel;
import io.github.oleIva.model.enums.PriorityEnum;
import io.github.oleIva.model.enums.StatusEnum;
import io.github.oleIva.utils.DatabaseFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Oleh Ivashko
 */
public class DaoImpl extends Queries implements Dao {

    @Override
    public TaskModel createTask(TaskModel task) {
        try (Connection connection = DatabaseFactory.createConnection()) {
            PreparedStatement pst = connection.prepareStatement(CREATE_TASK);
            int i = 0;
            pst.setString(++i, task.getName());
            pst.setTimestamp(++i, Timestamp.valueOf(task.getCompleteTime()));
            pst.setInt(++i, task.getPriority().ordinal() + 1);
            pst.setInt(++i, task.getTaskStatus().ordinal() + 1);
            pst.execute();

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return task;
    }

    @Override
    public TaskFinishedModel createCompleteTask(TaskFinishedModel taskFinish) {
        try (Connection connection = DatabaseFactory.createConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(CREATE_COMPLETE_TASK);

            int i = 0;
            preparedStatement.setString(++i, taskFinish.getName());
            preparedStatement.setTimestamp(++i, Timestamp.valueOf(taskFinish.getCompleteTime()));
            preparedStatement.setInt(++i, taskFinish.getPriority().ordinal() + 1);
            preparedStatement.setInt(++i, taskFinish.getTaskStatus().ordinal() + 1);
            preparedStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return taskFinish;
    }

    @Override
    public void deleteFromTasks(TaskModel task) {
        try (Connection connection = DatabaseFactory.createConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_TASK);
            int i = 0;
            preparedStatement.setInt(++i, task.getId());
            preparedStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public TaskModel getTask(int id) {
        try (Connection connection = DatabaseFactory.createConnection();
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(String.format(GET_TASK, id));
            resultSet.next();
            int i = 0;
            TaskModel task = new TaskModel();
            task.setId(id);
            task.setName(resultSet.getString(++i));
            task.setCompleteTime(resultSet.getTimestamp(++i).toLocalDateTime().withNano(0));
            task.setPriority(PriorityEnum.values()[resultSet.getInt(++i)]);
            task.setTaskStatus(StatusEnum.values()[resultSet.getInt(++i)]);
            return task;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public TaskModel updateTask(int id, TaskModel task) {
        try (Connection connection = DatabaseFactory.createConnection()) {
            int i = 0;
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TASK);
            preparedStatement.setString(++i, task.getName());
            preparedStatement.setTimestamp(++i, Timestamp.valueOf(task.getCompleteTime()));
            preparedStatement.setInt(++i, task.getPriority().ordinal() + 1);
            preparedStatement.setInt(++i, task.getTaskStatus().ordinal() + 1);
            preparedStatement.setInt(++i, id);
            preparedStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return task;
    }

    @Override
    public List<TaskModel> getAllTasks() {
        List<TaskModel> taskList = new ArrayList<>();
        try (Connection connection = DatabaseFactory.createConnection();
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(GET_ALL_TASKS);
            while (resultSet.next()) {
                int i = 0;
                TaskModel task = new TaskModel();
                task.setId(resultSet.getInt(++i));
                task.setName(resultSet.getString(++i));
                task.setCompleteTime(resultSet.getTimestamp(++i).toLocalDateTime().withNano(0));
                task.setPriority(PriorityEnum.values()[resultSet.getInt(++i) - 1]);
                task.setTaskStatus(StatusEnum.values()[resultSet.getInt(++i) - 1]);
                taskList.add(task);
            }
            return taskList;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public List<TaskFinishedModel> getAllFinishedTasks() {
        List<TaskFinishedModel> taskList = new ArrayList<>();

        try (Connection connection = DatabaseFactory.createConnection();
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(GET_ALL_TASKS_FINISH);

            while (resultSet.next()) {
                int i = 0;
                TaskFinishedModel task = new TaskFinishedModel();
                task.setId(resultSet.getInt(++i));
                task.setName(resultSet.getString(++i));
                task.setCompleteTime(resultSet.getTimestamp(++i).toLocalDateTime().withNano(0));
                task.setPriority(PriorityEnum.values()[resultSet.getInt(++i) - 1]);
                task.setTaskStatus(StatusEnum.values()[resultSet.getInt(++i) - 1]);

                taskList.add(task);
            }
            return taskList;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

}
