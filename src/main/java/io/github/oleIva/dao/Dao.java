package io.github.oleIva.dao;

import io.github.oleIva.model.TaskFinishedModel;
import io.github.oleIva.model.TaskModel;

import java.util.List;

/**
 * @author Oleh Ivashko
 */
public interface Dao {

    TaskModel createTask(TaskModel task);

    TaskFinishedModel createCompleteTask(TaskFinishedModel task);

    TaskModel getTask(int id);

    TaskModel updateTask(int id, TaskModel task);

    List<TaskFinishedModel> getAllFinishedTasks();

    List<TaskModel> getAllTasks();

    void deleteFromTasks(TaskModel task);
}
