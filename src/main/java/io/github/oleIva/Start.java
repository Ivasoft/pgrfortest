package io.github.oleIva;

import io.github.oleIva.view.StartView;

/**
 * Main class for start application
 * @author Oleh Ivashko
 */

public class Start {
    public static void main(String[] args) {
        StartView startView = new StartView();
        startView.view();
    }

}