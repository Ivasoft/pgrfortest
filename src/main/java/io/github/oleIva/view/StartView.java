package io.github.oleIva.view;

import io.github.oleIva.dao.DaoImpl;
import io.github.oleIva.model.TaskModel;
import io.github.oleIva.model.enums.PriorityEnum;
import io.github.oleIva.services.TaskService;
import io.github.oleIva.services.TaskServiceImpl;

import java.time.LocalDateTime;

/**
 * @author Oleh Ivashko
 */
public class StartView {

    public void view() {
        TaskService taskService = new TaskServiceImpl(new DaoImpl());
        try (MasterView masterView = new MasterView()) {

            boolean exit = false;
            while (!exit) {
                masterView.printMainMenu();
                int menuItem = masterView.chooseMenu();
                if (menuItem == 1) { //--> New task
                    masterView.clean();
                    String taskName = masterView.getTaskName();
                    LocalDateTime completeDate = masterView.getTaskCompleteDate();
                    PriorityEnum taskPriority = masterView.getPriority();
                    TaskModel task = taskService.createTask(taskName, completeDate, taskPriority);
                    masterView.clean();
                    System.out.println("Created TaskModel:" + task);

                } else if (menuItem == 2) {
                    masterView.clean();
                    try {
                        masterView.printTaskList(taskService.getAllTasks());
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("TaskModel list empty.");
                        continue;
                    }
                    masterView.printTaskListMenu();
                    int taskListItem = masterView.chooseMenu();

                    if (taskListItem == 1) {// -> Choose task
                        int id = masterView.chooseIdForComplete();
                        taskService.completeTask(id);
                        System.out.println("Completed Task");

                    } else if (taskListItem == 2) {// -> All completed Tasks List
                        masterView.clean();
                        try {
                            masterView.printFinishTask(taskService.getAllCompleteTasks());
                            masterView.printCompleteTaskListMenu();
                            masterView.chooseMenu();
                        } catch (Exception e) {
                            System.out.println("Complete TaskModel list empty.");
                        }
                    }
                }else
                {exit = true;}
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
