package io.github.oleIva.view;

import io.github.oleIva.model.TaskFinishedModel;
import io.github.oleIva.model.TaskModel;
import io.github.oleIva.model.enums.PriorityEnum;
import io.github.oleIva.utils.DataFormatPojo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Oleh Ivashko
 */
public class MasterView implements AutoCloseable {

    private String DELIMITER = "-----------------------------------------------------------------------";
    private String ADDTASK = "Add task";
    private String SHOW_LIST = "Show list of tasks";
    private String EXIT = "Exit";
    private String RETURN = "Return to main Menu";
    private String FORMAT = "1986-06-05 08:00";

    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public void printMainMenu() {
        System.out.println("1. " + ADDTASK);
        System.out.println("2. " + SHOW_LIST);
        System.out.println("3. " + EXIT);
        System.out.println(DELIMITER);
    }

    public void printTaskList(List<TaskModel> taskList) {
        System.out.printf("|%s|%s|%s|%s|%s|\n",
                corSpace(5, "Id"),
                corSpace(22, "TaskModel name"),
                corSpace(18, "CompleteTime"),
                corSpace(10, "Priority"),
                corSpace(10, "State"));

        System.out.println(DELIMITER);
        for (TaskModel task : taskList) {
            System.out.printf("|%s|%s|%s|%s|%s|\n",
                    corSpace(5, String.valueOf(task.getId())),
                    corSpace(22, task.getName()),
                    corSpace(18, DataFormatPojo.getFormattedDate(task.getCompleteTime())),
                    corSpace(10, task.getPriority().toString()),
                    corSpace(10, task.getTaskStatus().toString()));
        }
        System.out.println("");
    }

    public void printFinishTask(List<TaskFinishedModel> taskList) {
        System.out.printf("|%s|%s|%s|%s|%s|\n",
                corSpace(5, "Id"),
                corSpace(22, "TaskModel name"),
                corSpace(18, "CompleteTime"),
                corSpace(10, "Priority"),
                corSpace(10, "State"));

        System.out.println(DELIMITER);
        for (TaskFinishedModel task : taskList) {
            System.out.printf("|%s|%s|%s|%s|%s|\n",
                    corSpace(5, String.valueOf(task.getId())),
                    corSpace(22, task.getName()),
                    corSpace(18, DataFormatPojo.getFormattedDate(task.getCompleteTime())),
                    corSpace(10, task.getPriority().toString()),
                    corSpace(10, task.getTaskStatus().toString()));
        }
        System.out.println("");
    }

    public void printTaskListMenu() {
        System.out.println("1. Choose Task for Complete");
        System.out.println("2. Show all completed Tasks");
        System.out.println("3. " + RETURN);
    }

    public void printCompleteTaskListMenu() {
        System.out.println("1. " + RETURN);
    }

    public String getStringInput() {
        try {
            return reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getTaskName() {
        System.out.println("Enter task name:");
        return getStringInput();
    }

    public LocalDateTime getTaskCompleteDate() {
        System.out.println("Enter task time for complete. Format: " + FORMAT);
        while (true) {
            try {
                String inputTime = getStringInput();
                return DataFormatPojo.createDate(inputTime);
            } catch (Exception e) {
                System.out.println("Input Data incorrect. Format: " + FORMAT);
            }
        }
    }

    public PriorityEnum getPriority() {
        System.out.println("Choose task priority:");
        System.out.println("1. HIGH");
        System.out.println("2. NORMAL");
        System.out.println("3. LOW");

        PriorityEnum taskPriority;
        int menuItem = chooseMenu();
        if (menuItem == 1) {
            return PriorityEnum.HIGH;
        } else if (menuItem == 2) {
            return PriorityEnum.NORMAL;
        } else {
            return PriorityEnum.LOW;
        }
    }

    public int chooseMenu() {

        System.out.println("Please make your choice!");
        String input = getStringInput();
        while (true) {
            try {
                return Integer.parseInt(input);
            } catch (NumberFormatException e) {
                System.out.println("Please enter correct number");
            }
        }
    }

    public int chooseIdForComplete() {
        System.out.println("Choose task id for complete");
        return chooseMenu();
    }

    public void clean() {
        System.out.println("");
    }

    @Override
    public void close() throws Exception {
        reader.close();
    }

    private String corSpace(int width, String value) {
        StringBuilder sb = new StringBuilder(" " + value);
        for (int i = 0; i < width - value.length() - 1; i++) {
            sb.append(" ");
        }
        return sb.toString();
    }
}
