
SET FOREIGN_KEY_CHECKS = 0;
CREATE TABLE Statuses(

  status_id INT AUTO_INCREMENT,
  name VARCHAR (15),
  PRIMARY KEY (status_id)
);

CREATE TABLE Priorities(

  priority_id INT AUTO_INCREMENT,
  name VARCHAR (15),
  PRIMARY KEY (priority_id)
);

CREATE TABLE Tasks(

  id INT AUTO_INCREMENT,
  name VARCHAR (25),
  complete_date TIMESTAMP,
  priority_id INT NOT NULL,
  status_id INT NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (priority_id) REFERENCES Priorities(priority_id),
  FOREIGN KEY (status_id) REFERENCES Statuses(status_id)
);

CREATE TABLE TaskFinished(

  id INT AUTO_INCREMENT,
  name VARCHAR (25),
  complete_date TIMESTAMP,
  priority_id INT NOT NULL,
  status_id INT NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (priority_id) REFERENCES Priorities(priority_id),
  FOREIGN KEY (status_id) REFERENCES Statuses(status_id)
);


SET FOREIGN_KEY_CHECKS =1;

INSERT INTO Statuses (name) VALUES ('NEW');
INSERT INTO Statuses (name) VALUES ('COMPLETE');
INSERT INTO Statuses (name) VALUES ('OVERDUE');

INSERT INTO Priorities (name) VALUES ('HIGH');
INSERT INTO Priorities (name) VALUES ('NORMAL');
INSERT INTO Priorities (name) VALUES ('LOW');